class Product:
    def __init__(self, product_id, product_name, price, inventory_count):
        self.product_id=product_id
        self.product_name=product_name
        self.price=price
        self.inventory_count=inventory_count
        
    def sell(self, quantity):
        self.inventory_count -=quantity
        
    def apply_discount(self,discount_percentage):
        self.price-=(self.price * (discount_percentage / 100))
    
class DynamicPricing:
    def new_price(self, product, new_price):
        product.price = new_price       

product1=Product("001", "computer",1000,10)
print(f"Product: {product1.product_name}, Price: {product1.price}$, Available balance: {product1.inventory_count}")
product1.sell(5)
product1.apply_discount(10)
print("After sales and discounts :")
print(f"Product: {product1.product_name}, Price: {product1.price}$, Available balance: {product1.inventory_count}")
dyn_pricing=DynamicPricing()
dyn_pricing.new_price(product1,800)
print("After dynamic pricing :")
print(f"Product: {product1.product_name}, Price: {product1.price}$, Available balance: {product1.inventory_count}")
